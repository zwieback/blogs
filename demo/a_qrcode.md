二维码
==

* 在链接显示文本中使用`$qrcode([文本，可选]):`前缀定义二维码的标题（默认为显示文本），例如：

	```markdown
	[$qrcode:什么的已经够了啦](https://zwieback.gitee.io/blogs/?demo/index.md#二维码)
	```

	渲染为：  
	[$qrcode:什么的已经够了啦](https://zwieback.gitee.io/blogs/?demo/index.md#二维码)
