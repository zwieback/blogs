图片居中
==

* 在图片替代文本中使用`$center:`前缀声明图片居中，例如：

	```markdown
	![$center:](../resource/demo/example_img.png)
	```

	渲染为：

	![$center:](../resource/demo/example_img.png)
