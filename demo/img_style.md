图片样式
==

* 在图片替代文本中使用`$style(样式):`前缀定义图片的样式，例如：

	```markdown
	![$style(width:128px):](../resource/demo/example_img.png)
	```

	渲染为：

	![$style(width:128px):](../resource/demo/example_img.png)
