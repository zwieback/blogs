视频
==

* 在图片链接中使用`video://`协议声明视频，例如：

	```markdown
	![](video://https://interactive-examples.mdn.mozilla.net/media/cc0-videos/flower.mp4)
	```

	渲染为：

	![](video://https://interactive-examples.mdn.mozilla.net/media/cc0-videos/flower.mp4)
