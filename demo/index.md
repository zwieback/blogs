功能示例
==

`$include:pre_ref.md`
--

`$include:pre_latex.md`
--

`$include:pre_tags.md`
--

链接
--

### `$include:a_href.md`

### `$include:a_qrcode.md`

多媒体
--

### `$include:img_src.md`

### `$include:img_figcaption.md`

### `$include:img_style.md`

### `$include:img_center.md`

### `$include:img_audio.md`

### `$include:img_video.md`

### `$include:img_unzip.md`
