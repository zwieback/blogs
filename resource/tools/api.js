(function(){
	// MODIFIED BY WANGSHAN
	// 劫持new Date，使用指定的时间
	window.BMAP_NEW_DATE = function() {
		return window.BMAP_CONFIG && window.BMAP_CONFIG.DATE || new Date();
	};
	// END MODIFIED

	// MODIFIED BY WANGSHAN
	// 配置本地存储
	if (window.localStorage && typeof window.localStorage.setItem === "function") {
		let localStorages = window.BMAP_CONFIG && window.BMAP_CONFIG.LOCAL_STORAGES || { };
		Object.keys(localStorages).forEach(function(key) {
			window.localStorage.setItem(key, localStorages[key]);
		});
	}
	// END MODIFIED

	window.HOST_TYPE = "2";
	window.BMap_loadScriptTime = window.BMAP_NEW_DATE().getTime();
	window.BMap = window.BMap || { };
	window.BMap.apiLoad = function(){
		delete window.window.BMap.apiLoad;
		if (typeof window.onMapInitialized === "function") {
			window.onMapInitialized();
		}
	};
	var s = document.createElement("script");
	// MODIFIED BY WANGSHAN
	// 劫持getscript脚本，使用离线地图
	s.src = window.BMAP_CONFIG.BASE_URL + "getscript.js";
	// END MODIFIED
	document.body.appendChild(s);
})();