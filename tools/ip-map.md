# IP地址地图查询

<link rel="stylesheet" href="resource/tools/libpcapplusplus.css" type="text/css">

<div class="mb-3">
	<label for="ipinput" class="form-label">IP地址</label>
	<input id="ipinput" class="form-control" placeholder="请输入IP地址"></input>
</div>

<div id="map" style="width: 100%; height: calc(100vh - 250px);">

```script
// 配置IP地址匹配器，某些网站不允许上传mmdb文件
api.MaxMindMatcher.defaultDatabasePath =
	window.customizedReferences && window.customizedReferences.maxmindDefaultDatabasePath ||
	"resource/tools/GeoLite2-City_v6.mmdb";
api.MaxMindMatcher.asnDatabasePath =
	window.customizedReferences && window.customizedReferences.maxmindAsnDatabasePath ||
	"resource/tools/GeoLite2-ASN_v6.mmdb";
api.MaxMindMatcher.debug = true;
const IPMatcher = api.MaxMindMatcher;

// 初始化IP地址匹配器
const cityDatabase = IPMatcher.defaultDatabase();
const matcherPrototype = new IPMatcher({
	maxmind : cityDatabase
});

/**
 * 获取名称
 * @param {Object} data 数据
 * @param {Object.<string, string>} data.names 名称词典
 * @param {string[]} languages 语言列表
 * @returns {string} 名称
 */
function getName(data) {
	return api.MaxMindMatcher.VueComponent.methods.getName(data, [
		"zh-CN",
		"en"
	]);
}

// 结果字段处理器
const handlers = {
	continent : getName,
	country : getName,
	subdivisions : function(data) {
		return data && data.length ? getName(data[0]) : undefined
	},
	city : getName,
	location : function(data) {
		if (data)
			return "(" + data.longitude + ", " + data.latitude + ") " + data.time_zone;
	},
	organization : function(data) {
		if (data)
			return data.autonomous_system_organization + " (" + data.autonomous_system_number + ")";
	}
};

// 初始化地图
const BMAP_CONFIG = window.customizedConfig && window.customizedConfig.BMAP_CONFIG || { };
window.BMAP_CONFIG = BMAP_CONFIG;
const BMap = new Promise(function(resolve) {
	window.onMapInitialized = function() {
		resolve(window.BMapGL || window.BMap);
	};
});
if (!BMAP_CONFIG.AUTHENTIC_KEY)
	BMAP_CONFIG.BASE_URL = BMAP_CONFIG.BASE_URL || "resource/tools/";
loadScript(
	BMAP_CONFIG.BASE_URL ?
		BMAP_CONFIG.BASE_URL + "api.js" :
		"https://api.map.baidu.com/api?v=2.0&type=webgl&ak=" + BMAP_CONFIG.AUTHENTIC_KEY + "&callback=onMapInitialized",
	function() {
		BMap.then(function(BMap) {
			let map = new BMap.Map("map");
			map.enableScrollWheelZoom(true);
			map.centerAndZoom(new BMap.Point(0, 0), 1);

			// 监听IP地址输入框变化
			document.getElementById("ipinput").oninput = function(event) {
				let matcher = matcherPrototype.clone();
				let startTime = new Date().getTime();
				let value = event.target.value;
				matcher.lookup(value).then(function(result) {
					if (event.target.value !== value)
						return;
					map.clearOverlays();
					if (result.location && result.location.latitude && result.location.longitude) {
						let point = new BMap.Point(result.location.longitude, result.location.latitude);
						map.addOverlay(new BMap.Marker(point));
						map.centerAndZoom(point, 1);
						let textContent = "";
						Object.keys(handlers).forEach(function(key) {
							let value = handlers[key](result[key]);
							if (value) {
								if (textContent)
									textContent += "<br/>";
								textContent += key + ": " + value;
							}
						});
						map.openInfoWindow(new BMap.InfoWindow(textContent), point);
					}
				});
			};
		});
});

```
